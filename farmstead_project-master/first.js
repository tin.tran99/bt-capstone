//Thay đổi  moon/sun button
function changeBgIcon() {
  var x = document.getElementById("moon");
  var y = document.getElementById("sun");
  if (x.style.display === "block" && y.style.display === "none") {
    x.style.display = "none";
    y.style.display = "block";
  } else {
    x.style.display = "block";
    y.style.display = "none";
  }
}
// back to top function
//Get the button
var mybutton = document.getElementById("myBtn");
window.onscroll = function () {
  scrollFunction();
};
function scrollFunction() {
  var a = document.getElementById("moon");
  var b = document.getElementById("sun");
  // Đổi màu header bar theo màu background khi scroll
  if (a.style.display === "block" && b.style.display === "none") {
    header_bar.style.backgroundColor = "white";
    txt1.style.color = "black";
    txtTextColor3.style.color = "black";
    txtTextColor4.style.color = "black";
    // đè css hover background white
    var hoverText = document.getElementById("txt1");
    hoverText.addEventListener("mouseover", function () {
      hoverText.style.color = "black";
    });
    hoverText.addEventListener("mouseleave", function () {
      hoverText.style.color = "#fb811e";
    });
    //đè css hover 2
    var hoverText2 = document.getElementById("txtTextColor3");
    hoverText2.addEventListener("mouseover", function () {
      hoverText2.style.color = "#fb811e";
    });
    hoverText2.addEventListener("mouseleave", function () {
      hoverText2.style.color = "white";
    });
  } else {
    header_bar.style.backgroundColor = "#222";
    txt1.style.color = "white";
    // đè css hover background black
    var hoverText = document.getElementById("txt1");
    hoverText.addEventListener("mouseover", function () {
      hoverText.style.color = "white";
    });
    hoverText.addEventListener("mouseleave", function () {
      hoverText.style.color = "#fb811e";
    });
    //đè css hover 2
    var hoverText2 = document.getElementById("txtTextColor3");
    hoverText2.addEventListener("mouseover", function () {
      hoverText2.style.color = "#fb811e";
    });
    hoverText2.addEventListener("mouseleave", function () {
      hoverText2.style.color = "white";
    });
  }

  // check scroll
  if (
    document.body.scrollTop > 100 ||
    document.documentElement.scrollTop > 100
  ) {
    mybutton.style.display = "block";
    header_bar.style.padding = "0px";
    header_bar.style.boxShadow = "0 3px 6px 0 rgba(0, 0, 0, 0.05)";
    header_bar.style.transition = "all 0.3s ease-in-out";
    //đè css hover 2
    var hoverText2 = document.getElementById("txtTextColor3");
    hoverText2.addEventListener("mouseover", function () {
      hoverText2.style.color = "#fb811e";
    });
    hoverText2.addEventListener("mouseleave", function () {
      hoverText2.style.color = "black";
    });
  } else {
    mybutton.style.display = "none";
    header_bar.style.backgroundColor = "transparent";
    header_bar.style.padding = "15px";
    txtTextColor3.style.color = "white";
    txtTextColor4.style.color = "white";
    header_bar.style.boxShadow = "none";
    txt1.style.color = "#fb811e";
  }
}

// scroll to top function smooth
function scrollToTop() {
  var position = document.body.scrollTop || document.documentElement.scrollTop;
  if (position) {
    window.scrollBy(0, -Math.max(10, Math.floor(position / 10)));
    scrollAnimation = setTimeout("scrollToTop()", 10);
  } else clearTimeout(scrollAnimation);
}
// Đổi theme Dark/light
function swapStyleSheet(sheet) {
  document.getElementById("pagestyle").setAttribute("href", sheet);
}
// theme ban đầu
function initate() {
  var style1 = document.getElementById("sun");
  var style2 = document.getElementById("moon");

  style1.onclick = function () {
    swapStyleSheet("./bgLight.css");
  };
  style2.onclick = function () {
    swapStyleSheet("./bgdark.css");
  };
}
window.onload = initate;
